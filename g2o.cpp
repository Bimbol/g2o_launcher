#include "g2o.h"
#include "config.h"

#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>

#include <QMessageBox>
#include <QRegularExpression>
#include <QDir>
#include <QCoreApplication>
#include <QProcess>

#pragma comment(lib, "Version.lib")
#pragma comment(lib, "psapi.lib")

static QString getGothicExe()
{
    QDir version_dir(QCoreApplication::applicationDirPath());

    version_dir.cdUp();
    version_dir.cdUp();

    version_dir.cd("System");
    return version_dir.absoluteFilePath("Gothic2.exe");
}

static QString getNewestClientLibraryPath()
{
    QDir version_dir(QCoreApplication::applicationDirPath());
    static QRegularExpression regex_library_name(R"(^((\d+\.)*\d+)\.dll$)");

    version_dir.cdUp();
    version_dir.cd("version");

    QString newest_client;
    Version newest_client_version;

    for (const QFileInfo& entry : version_dir.entryInfoList(QDir::Files))
    {
        QString filename = entry.fileName().toLower();

        // Find the valid client dll
        QRegularExpressionMatch match_valid_dll_name = regex_library_name.match(filename);
        if (!match_valid_dll_name.hasMatch())
            continue;

        // Find the newest client dll version
        Version dll_version = Version::fromStringList(match_valid_dll_name.captured(1).split("."));
        if (dll_version < newest_client_version)
            continue;

        newest_client = filename;
        newest_client_version = dll_version;
    }

    if (newest_client.isEmpty())
        return "";

    return version_dir.filePath(newest_client);
}

static Version getDllVersion(const QString& dllPath)
{
    // Convert QString to a wide string for Windows API functions
    QByteArray byteArray = dllPath.toLocal8Bit();
    LPCSTR filePath = reinterpret_cast<LPCSTR>(byteArray.constData());

    DWORD versionHandle = 0;
    UINT size = GetFileVersionInfoSizeA(filePath, &versionHandle);

    if (size == 0)
        return Version();

    // Allocate buffer to hold version data
    QByteArray versionData(size, 0);

    // Retrieve the version information
    if (GetFileVersionInfoA(filePath, versionHandle, size, versionData.data()))
    {
        VS_FIXEDFILEINFO* fileInfo;
        UINT len = 0;

        // Query version information
        if (VerQueryValue(versionData.data(), L"\\", reinterpret_cast<LPVOID*>(&fileInfo), &len))
        {
            // Extract the version numbers from the fixed file info
            int major = (fileInfo->dwFileVersionMS >> 16) & 0xffff;
            int minor = (fileInfo->dwFileVersionMS) & 0xffff;
            int patch = (fileInfo->dwFileVersionLS >> 16) & 0xffff;
            int build = (fileInfo->dwFileVersionLS) & 0xffff;

            return Version { major, minor, patch, build };
        }
    }

    return Version();
}

static QDir getProcessPath(DWORD processID)
{
    wchar_t buffer[MAX_PATH];
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);

    if (!hProcess)
        return QDir();

    if (!GetModuleFileNameEx(hProcess, nullptr, buffer, MAX_PATH))
    {
        CloseHandle(hProcess);
        return QDir();
    }

    CloseHandle(hProcess);
    return QDir(QString::fromWCharArray(buffer));
}

namespace G2O {
    Version version()
    {
        return getDllVersion(getNewestClientLibraryPath());
    }

    QString versionText()
    {
        Version version = G2O::version();

        return QString("Version: %1.%2.%3.%4")
            .arg(version.major)
            .arg(version.minor)
            .arg(version.patch)
            .arg(version.build);
    }

    void run(ServerAddress address, QString nickname)
    {
        QStringList arguments;
        arguments << "--connect";
        arguments << address.toString();
        arguments << "--nickname ";
        arguments << nickname;

        QProcess process;
        if (!process.startDetached(getGothicExe(), arguments, QFileInfo(getGothicExe()).absolutePath()))
            QMessageBox::warning(nullptr, "G2O", "Failed to run Gothic2.exe!");
    }

    bool isRunning()
    {
        bool is_running = false;

        static PROCESSENTRY32W entry;
        entry.dwSize = sizeof(PROCESSENTRY32W);

        HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if (snapshot == INVALID_HANDLE_VALUE)
            return is_running;

        if (Process32FirstW(snapshot, &entry))
        {
            QDir gameDir = QDir(Config::get().getGameDir());

            do
            {
                if (_wcsicmp(entry.szExeFile, L"Gothic2.exe") != 0)
                    continue;

                QDir gameProcessPath = getProcessPath(entry.th32ProcessID);
                if (gameProcessPath.exists())
                    continue;

                gameProcessPath.cdUp(); // Remove "Gothic2.exe"
                gameProcessPath.cdUp(); // Go up from "System" to root dir

                is_running = gameProcessPath == gameDir;
                break;
            } while (Process32NextW(snapshot, &entry));
        }

        CloseHandle(snapshot);
        return is_running;
    }
}

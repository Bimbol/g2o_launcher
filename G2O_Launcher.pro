#-------------------------------------------------
#
# Project created by QtCreator 2017-08-09T20:39:32
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = G2O_Launcher
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += PROJECT_DIR=\\\"$$PWD\\\"

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32 {
    CONFIG += embed_manifest_exe
    QMAKE_LFLAGS += \"/MANIFESTUAC:level=\'requireAdministrator\' uiAccess=\'false\'\"
}

win32:RC_ICONS += resources/icon.ico

SOURCES += \
    g2o.cpp \
    main.cpp \
    launcher.cpp \
    serverlist.cpp \
    downloadmanager.cpp \
    httpmanager.cpp \
    autoupdater.cpp \
    servertreewidgetitem.cpp \
    updatepack.cpp \
    updatescript.cpp \
    config.cpp \
    servermanager.cpp \
    serveraddress.cpp \
    packets.cpp \
    servertreewidget.cpp \
    favoritelist.cpp \
    internetlist.cpp \
    dependencies/xamgu-qss-loader/Source/QsseFileLoader.cpp

HEADERS += \
    g2o.h \
    launcher.h \
    serverlist.h \
    pch.h \
    downloadmanager.h \
    httpmanager.h \
    autoupdater.h \
    servertreewidgetitem.h \
    updatepack.h \
    updatescript.h \
    version.h \
    config.h \
    servermanager.h \
    serveraddress.h \
    packets.h \
    servertreewidget.h \
    favoritelist.h \
    internetlist.h \
    dependencies/xamgu-qss-loader/Source/QsseFileLoader.h

FORMS += \
    launcher.ui \
    autoupdater.ui

win32 {
    LIBS += ws2_32.lib

    LIBS += -L$$PWD/dependencies/zlib/lib/ -lzlib
    INCLUDEPATH += $$PWD/dependencies/zlib/include
    DEPENDPATH += $$PWD/dependencies/zlib/include

    LIBS += -L$$PWD/dependencies/quazip/lib/ -lquazip
    INCLUDEPATH += $$PWD/dependencies/quazip/include
    DEPENDPATH += $$PWD/dependencies/quazip/include

    INCLUDEPATH += $$PWD/dependencies/xamgu-qss-loader/Source
}

RESOURCES += \
    resources.qrc

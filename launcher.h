#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMainWindow>
#include <QTimer>

#ifdef QT_DEBUG
#include <QFileSystemWatcher>
#endif

#include "autoupdater.h"
#include "internetlist.h"
#include "favoritelist.h"

namespace Ui {
class Launcher;
}

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = 0);
    ~Launcher();

    void init();

private slots:
    void onRefresh();

#ifdef QT_DEBUG
    void onStyleSheetReload();
#endif

private:
    void loadConfig();
    void loadAssets();

    Ui::Launcher *ui;
    AutoUpdater m_Updater;
    InternetList m_NetList;
    FavoriteList m_FavList;

#ifdef QT_DEBUG
    QFileSystemWatcher m_Watcher;
#endif
};

#endif // LAUNCHER_H

#include "servertreewidgetitem.h"
#include "internetlist.h"
#include "ui_launcher.h"

InternetList::InternetList(Ui::Launcher *ui) :
    ServerList(ui)
{
}

void InternetList::init()
{
    setupMenu();
    setupTree(pUi->treeInternet);

    // Remove margins so it looks much better
    pUi->tabInternet->layout()->setMargin(0);
    pUi->treeInternet->init();

    // Connections
    connect(&m_Manager, &ServerManager::serverInfo, this, &InternetList::onServerInfo);
    connect(&m_Manager, &ServerManager::serverDetails, this, &InternetList::onServerDetails);
    connect(&m_Http, &HttpManager::finished, this, &InternetList::onRequestFinished);
    connect(pUi->treeInternet, &ServerTreeWidget::itemDoubleClicked, this, &InternetList::onServerDoubleClicked);
    connect(pUi->treeInternet, &ServerTreeWidget::itemSelectionChanged, this, &InternetList::onServerSelectionChanged);
    connect(pUi->treeInternet, &ServerTreeWidget::enterKeyPressed, this, &InternetList::onServerKeyEnter);
    connect(pUi->treeInternet, &ServerTreeWidget::customContextMenuRequested, this, &InternetList::onContextMenu);
    connect(pUi->tabServers, &QTabWidget::currentChanged, this, &InternetList::onServerTabChanged);
    connect(m_pTimer, &QTimer::timeout, this, &InternetList::onTimeout);
}

void InternetList::refreshAll()
{
    pUi->treeInternet->clear();
    m_Http.get(QUrl("https://api.gothic-online.com/v2/master/servers/"));
}

void InternetList::setupMenu()
{
    m_ContextMenu.addAction("Connect to the server", this, SLOT(onConnect()));
    m_ContextMenu.addAction("Add the server to your favorites", this, SLOT(onFavoriteAdded()));
}

void InternetList::onConnect()
{
    auto currentItem = static_cast<ServerTreeWidgetItem*>(pUi->treeInternet->currentItem());
    if (currentItem)
        join(currentItem->getAddress(), currentItem->getVersion());
}

void InternetList::onFavoriteAdded()
{
    auto currentItem = static_cast<ServerTreeWidgetItem*>(pUi->treeInternet->currentItem());
    if (currentItem)
        emit favoriteAdded(currentItem->getAddress());
}

void InternetList::onTimeout()
{
    ServerList::onTimeout();

    // Fill servers that didn't respond
    for (auto address : m_Manager.failRequests())
        pUi->treeInternet->update(PacketInfo::unknown(address));
}

void InternetList::onRequestFinished(QUrl url, QJsonDocument doc)
{
    pUi->treeInternet->clear();

    QJsonArray jsonServers = doc.array();
    for (auto jsonServer : jsonServers)
    {
        QJsonObject obj = jsonServer.toObject();

        auto itAddress = obj.find("address");
        if (itAddress == obj.end()) continue;

        QStringList ip_port = (*itAddress).toString().split(':');

        ServerAddress address { ip_port[0], static_cast<ushort>((ip_port[1]).toInt()) };
        pUi->treeInternet->insert(PacketInfo::pinging(address));
    }

    ServerList::refreshAll(pUi->treeInternet);
}

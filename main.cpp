#include "launcher.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Launcher launcher;

    launcher.init();
    launcher.show();

    return app.exec();
}

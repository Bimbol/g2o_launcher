#include <QDomDocument>
#include <QDomNode>
#include <QDomElement>
#include <QTextStream>
#include <QInputDialog>
#include <QMessageBox>
#include <QFile>

#include "servertreewidgetitem.h"
#include "favoritelist.h"
#include "ui_launcher.h"

FavoriteList::FavoriteList(Ui::Launcher *ui) :
    ServerList(ui)
{
}

void FavoriteList::init()
{
    setupTree(pUi->treeFavorite);
    setupMenu();

    // Remove margins so it looks much better
    pUi->tabFavorite->layout()->setMargin(0);
    pUi->treeFavorite->init();

    // Connections
    connect(&m_Manager, &ServerManager::serverInfo, this, &FavoriteList::onServerInfo);
    connect(&m_Manager, &ServerManager::serverDetails, this, &FavoriteList::onServerDetails);
    connect(pUi->treeFavorite, &ServerTreeWidget::itemDoubleClicked, this, &FavoriteList::onServerDoubleClicked);
    connect(pUi->treeFavorite, &ServerTreeWidget::itemSelectionChanged, this, &FavoriteList::onServerSelectionChanged);
    connect(pUi->treeFavorite, &ServerTreeWidget::enterKeyPressed, this, &FavoriteList::onServerKeyEnter);
    connect(pUi->treeFavorite, &ServerTreeWidget::insertKeyPressed, this, &FavoriteList::onAdd);
    connect(pUi->treeFavorite, &ServerTreeWidget::deleteKeyPressed, this, &FavoriteList::onRemove);
    connect(pUi->treeFavorite, &ServerTreeWidget::customContextMenuRequested, this, &FavoriteList::onContextMenu);
    connect(pUi->tabServers, &QTabWidget::currentChanged, this, &FavoriteList::onServerTabChanged);
    connect(pUi->btnFavAdd, &QPushButton::released, this, &FavoriteList::onAdd);
    connect(pUi->btnFavEdit, &QPushButton::released, this, &FavoriteList::onEdit);
    connect(pUi->btnFavRemove, &QPushButton::released, this, &FavoriteList::onRemove);
    connect(m_pTimer, &QTimer::timeout, this, &FavoriteList::onTimeout);

    load();
}

void FavoriteList::refreshAll()
{
    QVector<ServerTreeWidgetItem*> rows;
    for (int i = 0; i < pUi->treeFavorite->topLevelItemCount(); ++i) {
        ServerTreeWidgetItem* row = static_cast<ServerTreeWidgetItem*>(pUi->treeFavorite->topLevelItem(i));
        rows.push_back(row);
    }

    for (auto row : rows)
        pUi->treeFavorite->update(PacketInfo::pinging(row->getAddress()), row);

    ServerList::refreshAll(pUi->treeFavorite);
}

void FavoriteList::load()
{
    QDomDocument doc;
    QFile file("favorite.xml");
    if (!file.open(QIODevice::ReadOnly) || !doc.setContent(&file))
        return;

    QDomNodeList servers = doc.elementsByTagName("server");
    for (int i = 0; i < servers.size(); ++i)
    {
        QDomNode server = servers.item(i);
        QDomElement ipAddr = server.firstChildElement("ip");
        QDomElement port = server.firstChildElement("port");

        if (ipAddr.isNull() || port.isNull())
            continue;

        ServerAddress address { ipAddr.text(), port.text().toUShort() };

        QString address_string = address.toString();
        if (m_addresses.find(address_string) != m_addresses.end())
            continue;

        pUi->treeFavorite->insert(PacketInfo::pinging(address));
        m_addresses.insert(address_string);
    }

    ServerList::refreshAll(pUi->treeFavorite);
}

void FavoriteList::save()
{
    QFile file("favorite.xml");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QDomDocument doc;
    QDomElement servers = doc.createElement("servers");

    for (int i = 0; i < pUi->treeFavorite->topLevelItemCount(); ++i) {
        ServerTreeWidgetItem* row = static_cast<ServerTreeWidgetItem*>(pUi->treeFavorite->topLevelItem(i));
        const ServerAddress& rowAddress = row->getAddress();

        QDomElement ipAddr = doc.createElement("ip");
        ipAddr.appendChild(doc.createTextNode(rowAddress.IP));
        QDomElement port = doc.createElement("port");
        port.appendChild(doc.createTextNode(QString::number(rowAddress.Port)));

        QDomElement server = doc.createElement("server");
        server.appendChild(ipAddr);
        server.appendChild(port);

        servers.appendChild(server);
    }

    doc.appendChild(servers);

    QTextStream outStream(&file);
    outStream << doc.toString();
    file.close();
}

void FavoriteList::setupMenu()
{
    m_ContextMenu.addAction("Connect to the server", this, SLOT(onConnect()));
}

void FavoriteList::onConnect()
{
    auto currentItem = static_cast<ServerTreeWidgetItem*>(pUi->treeFavorite->currentItem());
    if (currentItem)
        join(currentItem->getAddress(), currentItem->getVersion());
}

void FavoriteList::onFavoriteAdded(const ServerAddress& address)
{
    if (m_addresses.find(address.toString()) != m_addresses.end())
    {
        QMessageBox::warning(nullptr, "Favorite", "Server already added to favorites!");
        return;
    }

    QMessageBox::information(nullptr, "Favorite", "Server added to favorites!");
    pUi->treeFavorite->insert(PacketInfo::pinging(address));

    refreshOne(address);
    save();
}

void FavoriteList::onTimeout()
{
    ServerList::onTimeout();

    // Fill servers that didn't respond
    for (auto address : m_Manager.failRequests())
        pUi->treeFavorite->update(PacketInfo::unknown(address));
}

void FavoriteList::onAdd()
{
    bool ok;
    QString ipAddr = QInputDialog::getText(nullptr, "Add server", "Enter Host:Port", QLineEdit::Normal, "", &ok, Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    if (!ok || ipAddr.isEmpty())
        return;

    ServerAddress address = ServerAddress::parse(ipAddr);
    if (address.isUnassigned())
    {
        QMessageBox::warning(nullptr, "Favorite", "Invalid Host:Port!");
        return;
    }

    if (m_addresses.find(address.toString()) != m_addresses.end())
    {
        QMessageBox::warning(nullptr, "Favorite", "Server already added to favorites!");
        return;
    }

    pUi->treeFavorite->insert(PacketInfo::pinging(address));

    refreshOne(address);
    save();
}

void FavoriteList::onEdit()
{
    auto currentItem = static_cast<ServerTreeWidgetItem*>(pUi->treeFavorite->currentItem());
    if (currentItem == nullptr)
        return;

    bool ok;
    QString ipAddr = QInputDialog::getText(nullptr, "Edit server", "Enter Host:Port", QLineEdit::Normal, currentItem->getAddress().toString(), &ok, Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    if (!ok || ipAddr.isEmpty())
        return;

    if (currentItem->getAddress().toString() == ipAddr)
        return;

    ServerAddress address = ServerAddress::parse(ipAddr);
    if (address.isUnassigned())
    {
        QMessageBox::warning(nullptr, "Favorite", "Invalid Host:Port!");
        return;
    }

    if (m_addresses.find(address.toString()) != m_addresses.end())
    {
        QMessageBox::warning(nullptr, "Favorite", "Server already added to favorites!");
        return;
    }

    m_addresses.erase(m_addresses.find(currentItem->getAddress().toString()));
    currentItem->setAddress(address);
    m_addresses.insert(address.toString());

    pUi->treeFavorite->update(PacketInfo::pinging(address));

    refreshOne(address);
    save();
}

void FavoriteList::onRemove()
{
    auto currentIndex = pUi->treeFavorite->currentIndex();
    if (!currentIndex.isValid())
        return;

    auto currentItem = static_cast<ServerTreeWidgetItem*>(pUi->treeFavorite->takeTopLevelItem(currentIndex.row()));
    m_addresses.erase(m_addresses.find(currentItem->getAddress().toString()));

    delete currentItem;
    save();
}

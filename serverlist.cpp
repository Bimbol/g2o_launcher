#include <QMessageBox>

#include "servertreewidgetitem.h"
#include "serverlist.h"
#include "g2o.h"
#include "config.h"
#include "ui_launcher.h"

ServerList::ServerList(Ui::Launcher *ui, QObject *parent) :
    QObject(parent),
    pUi(ui)
{
    m_Manager.start(m_Thread);

    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(2000);
    m_pTimer->setSingleShot(true);
}

ServerList::~ServerList()
{
    m_Manager.stop();
    m_Thread.quit();
    m_Thread.wait();
    m_Thread.deleteLater();
}

void ServerList::refreshOne(const ServerAddress& address)
{
    m_Manager.clear();
    m_Manager.info(address);

    m_pTimer->start();
}

void ServerList::refreshAll(ServerTreeWidget* serverList)
{
    m_Manager.clear();

    for (int i = 0; i < serverList->topLevelItemCount(); ++i) {
        ServerTreeWidgetItem* row = static_cast<ServerTreeWidgetItem*>(serverList->topLevelItem(i));
        const ServerAddress& rowAddress = row->getAddress();

        m_Manager.info(rowAddress);
    }

    pUi->btnRefresh->clearFocus();
    pUi->btnRefresh->setEnabled(false);

    m_pTimer->start();
}

void ServerList::join(ServerAddress address, Version version)
{
    if (version.major == 0 && version.minor == 0 && version.patch == 0)
    {
        QMessageBox::warning(nullptr, "Server", "This server is unavailable!");
        return;
    }

    G2O::run(address, pUi->editNickname->text());
}

void ServerList::onTimeout()
{
    pUi->btnRefresh->setEnabled(true);
}

void ServerList::onServerInfo(PacketInfo packet)
{
    m_pServerTree->update(packet);
}

void ServerList::onServerDetails(PacketDetails packet)
{
    m_World = packet.world;

    pUi->viewWorld->setText("<b>World:</b> " + packet.world);
    pUi->viewDescription->setText(packet.description);
}

void ServerList::onServerSelectionChanged()
{
    auto selected_items = qobject_cast<QTreeWidget*>(sender())->selectedItems();
    if (selected_items.empty())
        return;

    auto& address = static_cast<ServerTreeWidgetItem*>( selected_items.first())->getAddress();
    m_Manager.details(address);
}

void ServerList::onServerDoubleClicked(QTreeWidgetItem *pItem, int id)
{
    Q_UNUSED(id);

    auto pServerItem = static_cast<ServerTreeWidgetItem*>(pItem);
    auto& address = pServerItem->getAddress();
    auto& version = pServerItem->getVersion();

    join(address, version);
}

void ServerList::onServerKeyEnter(QTreeWidgetItem *pItem)
{
    auto pServerItem = static_cast<ServerTreeWidgetItem*>(pItem);
    auto& address = pServerItem->getAddress();
    auto& version = pServerItem->getVersion();

    join(address, version);
}

void ServerList::onServerTabChanged(int id)
{
    Q_UNUSED(id);

    auto selected_items = (id == 0 ? pUi->treeInternet : pUi->treeFavorite)->selectedItems();
    if (selected_items.empty())
        return;

    auto& address = static_cast<ServerTreeWidgetItem*>(selected_items.first())->getAddress();
    m_Manager.details(address);
}

void ServerList::onContextMenu(const QPoint& point)
{
    auto item = static_cast<ServerTreeWidgetItem*>(m_pServerTree->itemAt(point));
    if (item) m_ContextMenu.exec(m_pServerTree->viewport()->mapToGlobal(point));
}

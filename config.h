#ifndef CONFIG_H
#define CONFIG_H

#include <QSettings>
#include <QString>

#include "serveraddress.h"

class Config
{
public:
    Config();

    static Config& get();

    QString getGameDir();

    QString getUpdateUUID();

    QString getNickname();
    void setNickname(const QString& nickname);

private:
    QSettings m_userRegistry;
};

#endif // CONFIG_H

#ifndef SERVERTREEWIDGETITEM_H
#define SERVERTREEWIDGETITEM_H

#include <QTreeWidgetItem>

#include "serveraddress.h"
#include "version.h"

class ServerTreeWidgetItem : public QTreeWidgetItem
{
public:
    explicit ServerTreeWidgetItem(QTreeWidget *treeview, int type = Type);

    inline const ServerAddress& getAddress() const { return m_Address; }
    inline const Version& getVersion() const { return m_Version; }
    inline const int getPlayers() const { return m_Players; }
    inline const int getPing() const { return m_Ping; }

    void setHostname(const QString& hostname);
    void setAddress(const ServerAddress& address);
    void setVersion(const QString& version);
    void setVersion(const Version& version, QString codeName);
    void setPlayers(const QString& players);
    void setPlayers(int current, int max);
    void setPing(const QString& ping);
    void setPing(int ping);

    virtual bool operator<(const QTreeWidgetItem &other) const override;

private:
    ServerAddress m_Address;
    Version m_Version { 0, 0, 0, 0 };

    int m_Players = 0;
    int m_Ping = 0;
};

#endif // SERVERTREEWIDGETITEM_H
